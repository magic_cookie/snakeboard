# Snakeboard

Python-driven imageboard for small communities.
![preview.png](https://bitbucket.org/repo/BBoBd4/images/1900501709-preview.png)

## Features

* Portable
* Invitecodes
* ImgurAPI image hosting
* NGINX rewrite url support
* IRC broadcasting

## Dependencies

* argparse==1.2.1
* backports.ssl-match-hostname==3.4.0.2
* certifi==14.05.14
* requests==2.6.0
* wsgiref==0.1.2
* tornado==4.1 (optional)
* Twisted==15.0.0 (optional)
* zope.interface==4.1.2 (optional)


## Included components

* PeeWee [https://github.com/coleifer/peewee](https://github.com/coleifer/peewee)
* Tempita [https://github.com/gjhiggins/tempita](https://github.com/gjhiggins/tempita)
* bottle.py [http://bottlepy.org/](http://bottlepy.org/)
* jQuery insertAtCaret
* jQuery endless scroll
* jsSHA
* Kube