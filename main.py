#!/usr/bin/python
# -*- coding: utf-8 -*-
import os, sys

import pyimgur

from bottle import route, get, post, request, response, run, static_file, redirect, template, default_app, error, Bottle, hook, debug
import tempita
import datetime
import re
import json
import socket
from time import sleep
from hashlib import md5
from db_model import *
from config import *


bottle_app = Bottle()
debug(False)

im = pyimgur.Imgur(IMGUR_CONFIG_CLIENT_ID, IMGUR_CONFIG_CLEINT_SECRET)

filters = {
"urlize":"",
"postlink": lambda s, b: re.sub(re_postlink, sub_postlink(b), s),
"imgur": lambda x: json.loads(x)["link"],
"thumb": lambda x: imgur_thumb_link(x)
}

def imgur_thumb_link(l):
    l_dict = json.loads(l)
    link = l_dict['link']
    width = l_dict['width']
    height = l_dict['height']
    ch = link.split("/")[-1].split(".")[0]
    ext = "."+link.split(".")[-1]
    if height > 3 * width:
        th = ch + "b"
    else:
        th = ch + "t"
    link = "http://i.imgur.com/" + th + ext
    return link

re_postlink = re.compile(r'&gt;&gt;(\d+)', re.DOTALL)
sub_postlink = lambda b: lambda s: get_thread_id(b, s.group(1))

def get_thread_id(b, s):
    try:
        gett = get_board(b).get(get_board(b).id==s)
        thr = str(gett.parent or s)
        return '<a class="red getbox" href="'+PREFIX_URL+'/'+b+'/thread/'+thr+'/#'+s+'">&gt;&gt;'+s+'</a> '
    except:
        return '&gt;&gt;'+s

xss_escape = {
    '&': '&amp;',
    '>': '&gt;',
    '<': '&lt;',
    '"': '&#34;',
    "'": '&#39;'
}

re_xss = re.compile(r'([&<"\'>])')
xss_sub = lambda s: re_xss.sub(lambda m: xss_escape[m.group()], s)


re_markup = re.compile(r'\[(/?b|/?i|/?u|/?s|/?code)\]')
markup_sub = lambda s: re_markup.sub(lambda m: '<{0}>'.format(''.join(map(lambda _: _ if _!='s' else 'strike', m.group(1)))), s)

global screened_threads
screened_threads = []

@hook("before_request")
def new_db_conn():
    database.connect()

@bottle_app.get('/<boardname>/thread/<threadnum:int>')
@bottle_app.get('/<boardname>/thread/<threadnum:int>/')
@bottle_app.get('/<boardname>/thread/<threadnum:int>/<filename:path>')
def send_thread(threadnum, boardname = DEFAULT_BOARD, filename = DEFAULT_INDEX):
    prefix_url = PREFIX_URL
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        return saint_get_thread(threadnum, boardname)

    #if not valid_user(request.get_cookie('u')):
    #   return show_login_screen()

    if threadnum in screened_threads:
        return error404(error)
    try:
        if USE_MEMCACHED:
            x = mem_cache.get("thread_"+boardname+"_"+str(threadnum))
            return x
        else:
            raise
    except:
        Posts = get_board(boardname)
        try:
            head_post = Posts.get(Posts.id == threadnum )
            if head_post.parent!=0: return error404(error)
        except:
            return error404(error)
        title = head_post.subject or "/ {0} /".format(boardname)
        description = head_post.subject or "#"+str(head_post.id)
        action = boardname + "/thread/"+str(threadnum)+'/'
        posts = Posts.select().where(Posts.parent == threadnum).order_by(Posts.timestamp)
        tmpl = tempita.Template.from_filename(DEFAULT_ROOT + DEFAULT_THREAD_INDEX, namespace=filters, encoding=None)
        if USE_MEMCACHED:
            mem_cache.set("thread_"+boardname+"_"+str(threadnum) , tmpl.substitute(locals()))
            return mem_cache.get("thread_"+boardname+"_"+str(threadnum))
        else:
            return tmpl.substitute(locals())

@bottle_app.post("/<boardname>/thread/<threadnum:int>/")
def submit_post(threadnum, boardname = DEFAULT_BOARD):
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        return saint_post_thread(threadnum, boardname)

    #if not valid_user(request.get_cookie("u")):
    #   return show_login_screen()  

    if check_banned(request.remote_addr):
        return banned_page()

    if threadnum in screened_threads:
        return error405(error)
    try:
        Posts = get_board(boardname)
        update_parent = Posts.get(Posts.id == threadnum)
        if update_parent.parent!=0: return error405(error)
    except:
        return error405(error)
    timestamp = datetime.datetime.now()
    ip = request.remote_addr
    if check_replying(ip, timestamp, threadnum, boardname): return oops(check_replying(ip, timestamp, threadnum, boardname))
    bumped = timestamp
    name = xss_sub(request.forms.get("name")).strip()
    name = (lambda x: x or DEFAULT_NAME)(name)
    subject = xss_sub(request.forms.get("subject")).strip()
    message = markup_sub(xss_sub(request.forms.get("message")).strip())
    #message = xss_sub(request.forms.get("message")).strip()
    (lambda x: redirect(REDIRECT_ROOT+"/"+boardname+"/thread/"+str(threadnum)+"/") if x=="" else None)(message)
    password = xss_sub(request.forms.get("password") or "" )
    if Posts.select().where(Posts.parent == threadnum).count() < BUMP_LIMIT and update_parent.bumped<timestamp:
        update_parent.bumped = timestamp; update_parent.save()

    if len(name)>TXT_NAME: return oops("Name is too long ({0})".format(str(len(name))))
    elif len(subject)>TXT_SUBJECT: return oops("Subject is too long ({0})".format(str(len(subject))))
    elif len(message)>TXT_MESSAGE: return oops("Message is too long ({0})".format(str(len(message))))

    upload = request.files.get("imagefile")
    if bool(upload):
        _, ext = os.path.splitext(upload.filename)
        if ext[1:].lower() not in UPLOAD_EXTENSIONS:
            return oops("File extension is not allowed")
        raw = upload.file.read() #FIXME check size, ddos, etc

        try:
            upload_data = upload_to_imgur(raw)
        except:
            return oops("File upload error")
    else:
        upload_data = ""

    Posts.create( parent = threadnum, timestamp = timestamp, bumped = bumped, ip = ip, name = name, 
        subject = subject, message = message, imagedata = upload_data )
    if USE_MEMCACHED:
        try:
            mem_cache.delete("thread_"+boardname+"_"+str(threadnum))
            mem_cache.delete(boardname)
        except:
            pass

        for page in xrange(*[1, MAX_PAGES]):
            try:
                mem_cache.delete("more_"+boardname+"_"+str(page))
            except:
                pass    
    redirect(REDIRECT_ROOT+"/"+boardname+"/thread/"+str(threadnum)+"/") 

@bottle_app.get("/more/<page:int>")
@bottle_app.get("/<boardname>/more/<page:int>")
def give_more(boardname = DEFAULT_BOARD, page = 1):
    prefix_url = PREFIX_URL
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        return saint_get_more(boardname, page)

    #if not valid_user(request.get_cookie("u")):
    #   return show_login_screen()
        
    if page + 1 > MAX_PAGES:    # Screens removed threads
        return ''
    try:
        if USE_MEMCACHED:
            x = mem_cache.get("more_"+boardname+"_"+str(page))
            return x
        else:
            raise
    except: 
        # на всякий случай. кеш перестраивается в app.get("/")
        print "!!! ВНЕЗАПНО"
        Posts = get_board(boardname)
        posts = Posts.select().where(Posts.parent == 0).order_by(Posts.bumped.desc()).paginate(page + 1, THREADS_PER_PAGE)
        labels = {}
        try:
            for p in posts:
                if p.bumped == datetime.datetime.max:
                    labels[p] = "locked"
                else:
                    labels[p] = ""
        except:
            database.connect()
            posts = Posts.select().where(Posts.parent == 0).order_by(Posts.bumped.desc()).paginate(page + 1, THREADS_PER_PAGE)
            labels = {}
            for p in posts:
                if p.bumped == datetime.datetime.max:
                    labels[p] = "locked"
                else:
                    labels[p] = ""                      
        replies_query = [ Posts.select().where(Posts.parent == post.id).order_by(Posts.timestamp).offset(theta(answers_count(boardname, post.id) - SHOW_ANSWERS)) for post in posts ]
        ommited = dict(zip([post.id for post in posts], [ theta_none(answers_count(boardname, post.id) - SHOW_ANSWERS) for post in posts]))
        replies = dict(zip([post.id for post in posts], replies_query))
        tmpl = tempita.Template.from_filename(DEFAULT_ROOT + DEFAULT_MORE, namespace=filters, encoding=None)
        if USE_MEMCACHED:
            mem_cache.set("more_"+boardname+"_"+str(page), tmpl.substitute(locals()))
            return mem_cache.get("more_"+boardname+"_"+str(page))
        else:
            return tmpl.substitute(locals())
        # /всякийслучай

def saint_get_more(boardname, page):
    prefix_url = PREFIX_URL
    Posts = get_board(boardname)    
    posts = Posts.select().where(Posts.parent == 0).order_by(Posts.bumped.desc()).paginate(page + 1, THREADS_PER_PAGE)
    labels = {}
    try:
        for p in posts:
            if p.bumped == datetime.datetime.max:
                labels[p] = "locked"
            elif page + 1 > MAX_PAGES:
                labels[p] = "deleted"
            else:   
                labels[p] = ""
    except:
        database.connect()
        posts = Posts.select().where(Posts.parent == 0).order_by(Posts.bumped.desc()).paginate(page + 1, THREADS_PER_PAGE)                  
        labels = {}
        for p in posts:
            if p.bumped == datetime.datetime.max:
                labels[p] = "locked"
            elif page + 1 > MAX_PAGES:
                labels[p] = "deleted"
            else:   
                labels[p] = ""
    replies_query = [ Posts.select().where(Posts.parent == post.id).order_by(Posts.timestamp).offset(theta(answers_count(boardname, post.id) - SHOW_ANSWERS)) for post in posts ]
    ommited = dict(zip([post.id for post in posts], [ theta_none(answers_count(boardname, post.id) - SHOW_ANSWERS) for post in posts]))
    replies = dict(zip([post.id for post in posts], replies_query))
    admin_mode = True
    tmpl = tempita.Template.from_filename(DEFAULT_ROOT + DEFAULT_MORE, namespace=filters, encoding=None)
    return tmpl.substitute(locals())

theta = lambda moo: 0 if moo<0 else moo 
theta_none = lambda moo: None if moo<0 else moo 

@bottle_app.get('/')
@bottle_app.get('/<filename:path>')
@bottle_app.get('/<boardname>/')
@bottle_app.get('/<boardname>/<filename:path>')
def send_board(boardname = DEFAULT_BOARD, filename = DEFAULT_INDEX):
    prefix_url = PREFIX_URL
    print request.remote_addr, [_+":"+request.headers.get(_) for _ in request.headers]
    if filename in BOARDS: boardname = filename; filename = DEFAULT_INDEX # handling absense of the last "/"
    if filename == DEFAULT_INDEX:
        if valid_admin(request.get_cookie("g")):
            return saint_get_board(boardname)

        #if not valid_user(request.get_cookie("u")):
        #   return show_login_screen()
        scrolldive = True

        try:
            x = mem_cache.get(boardname)
            return x
        except:

            Posts = get_board(boardname)
            action = boardname+"/"
            title = "/ {0} /".format(boardname)
            description = BOARDS[boardname]["description"]
            posts = Posts.select().where(Posts.parent == 0).order_by(Posts.bumped.desc()).limit(THREADS_PER_PAGE)               
            labels = {}
            try:
                for p in posts:
                    if p.bumped == datetime.datetime.max:
                        labels[p] = "locked"
                    else:
                        labels[p] = ""
            except:
                database.connect()
                posts = Posts.select().where(Posts.parent == 0).order_by(Posts.bumped.desc()).limit(THREADS_PER_PAGE)
                for p in posts:
                    if p.bumped == datetime.datetime.max:
                        labels[p] = "locked"
                    else:
                        labels[p] = ""  
            replies_query = [ Posts.select().where(Posts.parent == post.id).order_by(Posts.timestamp).offset(theta(answers_count(boardname, post.id) - SHOW_ANSWERS)) for post in posts ]
            ommited = dict(zip([post.id for post in posts], [ theta_none(answers_count(boardname, post.id) - SHOW_ANSWERS) for post in posts]))
            replies = dict(zip([post.id for post in posts], replies_query))
            tmpl = tempita.Template.from_filename(DEFAULT_ROOT + filename, namespace=filters, encoding=None)
            if USE_MEMCACHED:
                mem_cache.set(boardname , tmpl.substitute(locals()))
                try:
                    mem_cache.get("more_"+boardname+"_1")
                except :        
                    rebuild_more_cache(boardname)
                return mem_cache.get(boardname)
            else:
                return tmpl.substitute(locals())
            
    else:
        try:
            ext = filename.split(".")[-1]
            if ext in EXT_ALLOWED:  #SAFEREAD
                return static_file(filename, root=DEFAULT_ROOT)
            else:
                return error404(error)
        except:
            return error404(error)

# rebuild 'more' cache ---
def rebuild_more_cache(boardname):
    prefix_url = PREFIX_URL
    print "rebuilding 'more' cache", boardname
    Posts = get_board(boardname)
    for page in xrange(*[1, MAX_PAGES]):
        posts = Posts.select().where(Posts.parent == 0).order_by(Posts.bumped.desc()).paginate(page + 1, THREADS_PER_PAGE)
        labels = {}
        try:
            for p in posts:
                if p.bumped == datetime.datetime.max:
                    labels[p] = "locked"
                else:
                    labels[p] = ""
        except:
            database.connect()
            posts = Posts.select().where(Posts.parent == 0).order_by(Posts.bumped.desc()).paginate(page + 1, THREADS_PER_PAGE)
            labels = {}
            for p in posts:
                if p.bumped == datetime.datetime.max:
                    labels[p] = "locked"
                else:
                    labels[p] = ""
        replies_query = [ Posts.select().where(Posts.parent == post.id).order_by(Posts.timestamp).offset(theta(answers_count(boardname, post.id) - SHOW_ANSWERS)) for post in posts ]
        ommited = dict(zip([post.id for post in posts], [ theta_none(answers_count(boardname, post.id) - SHOW_ANSWERS) for post in posts]))
        replies = dict(zip([post.id for post in posts], replies_query))
        tmpl = tempita.Template.from_filename(DEFAULT_ROOT + DEFAULT_MORE, namespace=filters, encoding=None)
        mem_cache.set("more_"+boardname+"_"+str(page), tmpl.substitute(locals()))
    print "rebuilt", boardname
#------         

@bottle_app.post("/")
@bottle_app.post("/<boardname>")
@bottle_app.post("/<boardname>/")
def submit_post(boardname = DEFAULT_BOARD):
    prefix_url = PREFIX_URL
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        return saint_post_board(boardname)
        
    #if not valid_user(request.get_cookie("u")):
    #   return show_login_screen()  

    if check_banned(request.remote_addr):
        return banned_page()
        
    Posts = get_board(boardname)
    timestamp = datetime.datetime.now()
    ip = request.remote_addr
    if check_wipe(ip, timestamp, boardname): return oops(check_wipe(ip, timestamp, boardname))
    bumped = timestamp
    name = xss_sub(request.forms.get("name")).strip()
    name = (lambda x: x or DEFAULT_NAME)(name)
    subject = xss_sub(request.forms.get("subject")).strip()
    message = markup_sub(xss_sub(request.forms.get("message")).strip())
    #message = xss_sub(request.forms.get("message")).strip()
    (lambda x: redirect(REDIRECT_ROOT+"/"+boardname+"/") if x=="" else None)(message)
    password = xss_sub (request.forms.get("password") or "" )

    if len(name)>TXT_NAME: return oops("Name is too long ({0})".format(str(len(name))))
    elif len(subject)>TXT_SUBJECT: return oops("Subject is too long ({0})".format(str(len(subject))))
    elif len(message)>TXT_MESSAGE: return oops("Message is too long ({0})".format(str(len(message))))

    upload = request.files.get("imagefile")
    if bool(upload):
        _, ext = os.path.splitext(upload.filename)
        if ext[1:].lower() not in UPLOAD_EXTENSIONS:
            return oops("File extension is not allowed")
        raw = upload.file.read() #FIXME check size, ddos, etc

        try:
            upload_data = upload_to_imgur(raw)
        except:
            return oops("File upload error")
    else:
        upload_data = ""

    if USE_IRC_BOT: ircbot.get_instance().send_stream("\x02[\x1f{0}\x1f] \x034{1}\x03\x02 {2}".format(boardname, subject, message[:210]) )
    Posts.create( parent = 0, timestamp = timestamp, bumped = bumped, ip = ip, name = name,
         subject = subject, message = message, imagedata = upload_data )
    global screened_threads
    screened_objects = Posts.select().where(Posts.parent == 0).order_by(Posts.bumped.desc()).offset(THREADS_PER_PAGE * MAX_PAGES)
    screened_threads = [ _.id for _ in screened_objects]
    (lambda x: filter(lambda x: Posts.get(Posts.id == x ).delete_instance() and Posts.delete().where(Posts.parent == x ).execute(), screened_objects ) if (x-THREADS_PER_PAGE * MAX_PAGES)>CLEAR_DB else None)(threads_count(boardname))
    if USE_MEMCACHED:
        try:
            mem_cache.delete(boardname)
        except:
            pass    
        for page in xrange(*[1, MAX_PAGES]):
            try:
                mem_cache.delete("more_"+boardname+"_"+str(page))
            except:
                pass
    redirect(REDIRECT_ROOT+"/"+boardname+"/")

@bottle_app.route(ADMIN_PATH, method = ['GET', 'POST'])
def admin_login():
    prefix_url = PREFIX_URL
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        redirect(REDIRECT_ROOT+"/")
    else:
        action = ADMIN_PATH[1:]
        try:
            name = xss_sub (request.forms.get("name")).strip()
            password = xss_sub (request.forms.get("password"))
            salt = SALT
            if (name or password) != "":
                mdp = myd5(salt+name+myd5(password)+salt)
                if valid_admin(mdp):
                    response.set_cookie("g",mdp)
                    return send_board(DEFAULT_INDEX)
                else:
                    return oops("Invalid user or password") 
            else:
                return oops("Please, complete the fields")
        except Exception, e:
            print e
            pass
        title = "Админка"   
        tmpl = tempita.Template.from_filename(DEFAULT_ROOT+DEFAULT_ADMINKA, namespace=filters)
        return tmpl.substitute(locals())

@bottle_app.get("/x/banlist")
def banlist():
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        try:
            prefix_url = PREFIX_URL
            title = "Bans"
            boardname = "#Banlist"
            description = "banlist"
            action = "none"
            admin_mode = True
            bans = Bans.select()
            tmpl = tempita.Template.from_filename(DEFAULT_ROOT+"banlist.html", namespace=filters)
            return tmpl.substitute(locals())
        except Exception, e:
            print e
            return error404(error)
    else:
        return error404(error)

@bottle_app.get("/x/invites")
def invitelist():
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        try:
            prefix_url = PREFIX_URL
            title = "Invites"
            boardname = "#invites"
            description = "wowow"
            action = "none"
            admin_mode = True
            invites = Invites.select()
            tmpl = tempita.Template.from_filename(DEFAULT_ROOT+"invites.html", namespace=filters)
            return tmpl.substitute(locals())
        except Exception, e:
            print e
            return error404(error)
    else:
        return error404(error)

@bottle_app.post("/x/addinv")
def addinv():
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        todo = request.json['todo']
        invitecode = request.json['invitecode']
        if todo == "generate":
            try:
                Invites.create(invitecode = invitecode, taken = False,
                    expires = datetime.datetime.now()+datetime.timedelta(days=30))
                return '{ "status" : "added" }'
            except:
                return '{ "status" : "error" }'
        elif todo == "remove":
            try:
                invite = Invites.get(Invites.invitecode == invitecode)
                invite.delete_instance()
                return '{ "status" : "removed" }'
            except:
                return '{ "status" : "error" }'
        else:
            return '{ "status" : "error" }'
    else:
        return error404(error)

@bottle_app.get("/x/users")
def userlist():
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        try:
            prefix_url = PREFIX_URL
            title = "Users"
            boardname = "#Users"
            description = "users"
            action = "none"
            admin_mode = True
            users = Users.select()
            tmpl = tempita.Template.from_filename(DEFAULT_ROOT+"users.html", namespace=filters)
            return tmpl.substitute(locals())
        except Exception, e:
            print e
            return error404(error)
    else:
        return error404(error)

@bottle_app.post("/x/reguser")
def reguser():
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    invitecode = request.json['invitecode']
    if valid_invite(invitecode):
        login = request.json["login"]
        userhash = request.json["userhash"]
        try:
            new_user(login, userhash)
            delete_invite(invitecode)
            return '{ "status" : "registered" }'
        except:
            return '{ "status" : "error" }'
    else:
        return error404(error)

@bottle_app.post("/x/remuser")
def reguser():
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        try:
            user_id = int(request.json["user_id"])
            delete_user(user_id)
            return '{ "status" : "success" }'
        except Exception, e:
            return '{"status" : "{0}"}'.format(str(e))
    else:
        return error404(error)

@bottle_app.post("/x/ban")
def ban():
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        ip_start = xss_sub(request.json['ip_start'].strip())
        ip_stop = xss_sub(request.json['ip_stop'].strip())
        exp = xss_sub(request.json['exp'])
        try:
            socket.inet_aton(ip_start)
            socket.inet_aton(ip_stop)
        except socket.error, e:
            return '{"status" : "{0}"}'.format(str(e))

        if ip_start > ip_stop:
            return '{"status" : "wat?"}'

        if exp == "never":
            expires = datetime.datetime.max
        else:
            expires = datetime.datetime.now() + request.forms.get("exp")
        try:
            Bans.create( ip_start = ip_start, ip_stop = ip_stop, expires = expires )
            print "Banned", ip_start, "to", ip_stop
            return '{"status" : "banned permanently"}'
        except Exception, e:
            return '{"status" : "{0}"}'.format(str(e))
    else:
        return error404(error)

@bottle_app.post("/x/remban")
def reguser():
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        try:
            ban_id = int(request.json["ban_id"])
            delete_ban_record(ban_id)
            return '{ "status" : "success" }'
        except Exception, e:
            return '{"status" : "{0}"}'.format(str(e))
    else:
        return error404(error)      

''' #FIXME
@bottle_app.post("/x/unban")
def unban():
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        ip = request.json['ip'].strip()
        if ip == "":
            return '{"status" : "hmm..."}'
        try:
            q = Bans.get( Bans.ip == ip )
            q.delete_instance()
            return '{"status" : "unbanned"}'
        except:
            return '{"status" : "ip is not banned"}'
    else:
        return error404(error)
'''     

@bottle_app.post("/x/rem")
def rem():
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        boardname = request.json['board']
        psto = request.json['id']
        print "removing", psto, boardname
        try:
            Posts = get_board(boardname)
            q = Posts.delete().where( (Posts.parent == psto) | (Posts.id == psto) )
            q.execute()
        except:
            return '{ "status" : "error" }'
        return '{ "status" : "deleted" }'
    else:
        return error404(error)

@bottle_app.get("/x/bot")
def userlist():
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        try:
            prefix_url = PREFIX_URL
            title = "Bot"
            boardname = "#Bot"
            description = "\(`_`)/"
            action = "none"
            admin_mode = True
            users = Users.select()
            tmpl = tempita.Template.from_filename(DEFAULT_ROOT+"bot.html", namespace=filters)
            return tmpl.substitute(locals())
        except Exception, e:
            print e
            return error404(error)
    else:
        return error404(error)      

@bottle_app.post("/x/botsend")
def bot():
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    if valid_admin(request.get_cookie("g")):
        line = request.json['line'].strip()
        try:
            ircbot.get_instance().send_stream(line)
            return '{"status" : "success"}'
        except Exception, e:
            return '{"status" : "{0}"}'.format(str(e))
    else:
        return error404(error)      

def valid_invite(i):
    try:
        invite = Invites.get(Invites.invitecode == i)
        return True
    except:
        return False

def delete_invite(i):
    try:
        inv = Invites.get(Invites.invitecode == i)
        inv.delete_instance()
    except Exception, e:
        print e
        pass    

def valid_user(cookie):
    try:
        user = Users.get(Users.userhash == cookie)
        return True
    except:
        return False

def new_user(name, userhash):
    try:
        Users.create(login = name, userhash = userhash)
    except Exception, e:
        print e
        pass

def delete_user(user_id):
    try:
        user = Users.get( Users.id == user_id )
        user.delete_instance()
    except Exception, e:
        print e

def delete_ban_record(ban_id):
    try:
        ban = Bans.get( Bans.id == ban_id )
        ban.delete_instance()
    except Exception, e:
        print e 

def show_login_screen():
    title = "login"
    prefix_url = PREFIX_URL
    action = "login"
    tmpl = tempita.Template.from_filename(DEFAULT_ROOT + "login.html", namespace=filters, encoding=None)
    return tmpl.substitute(locals())

@bottle_app.get("/invite")
def register_user():
    prefix_url = PREFIX_URL
    print request.remote_addr, [request.headers.get(_) for _ in request.headers]
    invite = xss_sub(request.query.i).strip()
    if not valid_invite(invite) or not invite:
        return error404(error)
    else:
        action = "new_lolka"
        title = "new user"
        tmpl = tempita.Template.from_filename(DEFAULT_ROOT+"reg.html", namespace=filters)
        return tmpl.substitute(locals())

def init_admins():
    global admins
    admins = []
    salt = SALT
    if ADMINS:
        for k,v in ADMINS.iteritems():
            admins.append(myd5(salt+k+v+salt))

def valid_admin(g):
    if g in admins:
        return True
    else:
        return False

def myd5(string):
    return md5(string).hexdigest()  

#===  Admin views  ==========================================

def saint_get_board(boardname):
    prefix_url = PREFIX_URL
    action = boardname+"/"
    Posts = get_board(boardname)    
    title = "/ {0} /".format(boardname)
    admin_mode = True
    scrolldive = True
    description = BOARDS[boardname]["description"]
    posts = Posts.select().where(Posts.parent == 0).order_by(Posts.bumped.desc()).limit(THREADS_PER_PAGE)
    labels = {}
    try:
        for p in posts:
            if p.bumped == datetime.datetime.max:
                labels[p] = "locked"
            else:
                labels[p] = ""
    except:
        database.connect()
        posts = Posts.select().where(Posts.parent == 0).order_by(Posts.bumped.desc()).limit(THREADS_PER_PAGE)
        labels = {}
        for p in posts:
            if p.bumped == datetime.datetime.max:
                labels[p] = "locked"
            else:
                labels[p] = ""  
    replies_query = [ Posts.select().where(Posts.parent == post.id).order_by(Posts.timestamp).offset(theta(answers_count(boardname, post.id) - SHOW_ANSWERS)) for post in posts ]
    ommited = dict(zip([post.id for post in posts], [ theta_none(answers_count(boardname, post.id) - SHOW_ANSWERS) for post in posts]))
    replies = dict(zip([post.id for post in posts], replies_query))
    tmpl = tempita.Template.from_filename(DEFAULT_ROOT + DEFAULT_INDEX, namespace=filters, encoding=None)
    return tmpl.substitute(locals())

def saint_post_board(boardname):
    Posts = get_board(boardname)
    timestamp = datetime.datetime.now()
    ip = ''
    tl = request.forms.get("threadlock")
    if tl=="locked": 
        bumped = datetime.datetime.max
    else: 
        bumped = timestamp
    name = xss_sub(request.forms.get("name")).strip()
    name = (lambda x: '<strong class="gold">'+x+'</strong>' if x else DEFAULT_NAME)(name)
    subject = xss_sub(request.forms.get("subject")).strip()
    message = markup_sub(xss_sub(request.forms.get("message")).strip())
    #message = xss_sub(request.forms.get("message")).strip()
    (lambda x: redirect(REDIRECT_ROOT+"/"+boardname+"/") if x=="" else None)(message)
    password = xss_sub (request.forms.get("password") or "" )

    upload = request.files.get("imagefile")
    if bool(upload):
        _, ext = os.path.splitext(upload.filename)
        if ext[1:].lower() not in UPLOAD_EXTENSIONS:
            return oops("File extension is not allowed")
        raw = upload.file.read() #FIXME check size, ddos, etc

        try:
            upload_data = upload_to_imgur(raw)
        except:
            return oops("File upload error")
    else:
        upload_data = ""
    if USE_IRC_BOT: ircbot.get_instance().send_stream("\x02[\x1f{0}\x1f] \x034{1}\x03\x02 {2}".format(boardname, subject, message[:210]) )
    Posts.create( parent = 0, timestamp = timestamp, bumped = bumped, 
        ip = ip, name = name, subject = subject, message = message, imagedata = upload_data ) 
    global screened_threads
    screened_objects = Posts.select().where(Posts.parent == 0).order_by(Posts.bumped.desc()).offset(THREADS_PER_PAGE * MAX_PAGES)
    screened_threads = [ _.id for _ in screened_objects]
    (lambda x: filter(lambda x: Posts.get(Posts.id == x ).delete_instance() and Posts.delete().where(Posts.parent == x ).execute(), screened_objects ) if (x-THREADS_PER_PAGE * MAX_PAGES)>CLEAR_DB else None)(threads_count(boardname))
    if USE_MEMCACHED:
        try:
            mem_cache.delete(boardname)
        except:
            pass
        for page in xrange(*[1, MAX_PAGES]):
            try:
                mem_cache.delete("more_"+boardname+"_"+str(page))
            except:
                pass
    redirect(REDIRECT_ROOT+"/"+boardname+"/")   

def saint_get_thread(threadnum, boardname):
    prefix_url = PREFIX_URL
    Posts = get_board(boardname)
    try:
        head_post = Posts.get(Posts.id == threadnum )
        if head_post.parent!=0: return error404(error)
    except:
        return error404(error)
    title = head_post.subject or "/ {0} /".format(boardname)
    description = head_post.subject or "#"+str(head_post.id)
    if threadnum in screened_threads:
        title +='(deleted)'
        description +='(deleted)'
    action = boardname + "/thread/"+str(threadnum)+'/'
    admin_thread_mode = True
    posts = Posts.select().where(Posts.parent == threadnum).order_by(Posts.timestamp)
    tmpl = tempita.Template.from_filename(DEFAULT_ROOT + DEFAULT_THREAD_INDEX, namespace=filters, encoding=None) 
    return tmpl.substitute(locals())

def saint_post_thread(threadnum, boardname):
    try:
        Posts = get_board(boardname)
        update_parent = Posts.get(Posts.id == threadnum)
        if update_parent.parent!=0: return error405(error)
    except:
        return error405(error)
    timestamp = datetime.datetime.now()
    ip = ''
    #if check_replying(ip, timestamp, threadnum, boardname): return oops(check_replying(ip, timestamp, threadnum, boardname))
    bumped = timestamp
    name = xss_sub(request.forms.get("name")).strip()
    name = (lambda x: '<strong class="gold">'+x+'</strong>' if x else DEFAULT_NAME)(name)
    subject = xss_sub(request.forms.get("subject")).strip()
    message = markup_sub(xss_sub(request.forms.get("message")).strip())
    #message = xss_sub(request.forms.get("message")).strip()
    (lambda x: redirect(REDIRECT_ROOT+"/"+boardname+"/thread/"+str(threadnum)+"/") if x=="" else None)(message)
    password = xss_sub(request.forms.get("password") or "" )
    if Posts.select().where(Posts.parent == threadnum).count() < BUMP_LIMIT and update_parent.bumped<timestamp:
        update_parent.bumped = timestamp; update_parent.save()

    #if len(name)>TXT_NAME: return oops("Name is too long ({0})".format(str(len(name))))
    #elif len(subject)>TXT_SUBJECT: return oops("Subject is too long ({0})".format(str(len(subject))))
    #elif len(message)>TXT_MESSAGE: return oops("Message is too long ({0})".format(str(len(message))))

    upload = request.files.get("imagefile")
    if bool(upload):
        _, ext = os.path.splitext(upload.filename)
        if ext[1:].lower() not in UPLOAD_EXTENSIONS:
            return oops("File extension is not allowed")
        raw = upload.file.read() #FIXME check size, ddos, etc

        try:
            upload_data = upload_to_imgur(raw)
        except:
            return oops("File upload error")
    else:
        upload_data = ""

    Posts.create( parent = threadnum, timestamp = timestamp, bumped = bumped, ip = ip,
     name = name, subject = subject, message = message, imagedata = upload_data )
    if USE_MEMCACHED:
        try:
            mem_cache.delete("thread_"+boardname+"_"+str(threadnum))
            mem_cache.delete(boardname)
        except:
            pass
        
        for page in xrange(*[1, MAX_PAGES]):
            try:
                mem_cache.delete("more_"+boardname+"_"+str(page))
            except:
                pass        
    redirect(REDIRECT_ROOT+"/"+boardname+"/thread/"+str(threadnum)+"/")

#=============================================

@bottle_app.error(404)
def error404(error):
    prefix_url = PREFIX_URL
    title = "404"
    notfound = True
    tmpl = tempita.Template.from_filename(DEFAULT_ROOT+DEFAULT_ERROR, namespace=filters)
    return tmpl.substitute(locals())

@bottle_app.error(405)
def error405(error):
    prefix_url = PREFIX_URL
    title = "405"
    notfound = False
    reason = "Error: 405 Method Not Allowed"
    tmpl = tempita.Template.from_filename(DEFAULT_ROOT+DEFAULT_ERROR, namespace=filters)
    return tmpl.substitute(locals())

def oops(r, nf = False):
    title = "Woops!"
    prefix_url = PREFIX_URL
    reason = r
    notfound = nf
    tmpl = tempita.Template.from_filename(DEFAULT_ROOT+DEFAULT_ERROR, namespace=filters)
    return tmpl.substitute(locals())

def banned_page():
    title = "BANNED"
    prefix_url = PREFIX_URL
    tmpl = tempita.Template.from_filename(DEFAULT_ROOT+DEFAULT_BANNED, namespace=filters)
    return tmpl.substitute(locals())

def check_wipe(ip, timestamp, boardname):
    Posts = get_board(boardname)
    l = Posts.select().where( Posts.parent == 0 ).order_by(Posts.timestamp.desc())
    o = l.where(Posts.ip == ip).limit(THREAD_RATE_LIMIT_IP)
    h = l.limit(THREAD_RATE_LIMIT)
    try:
        if (o.count()>=THREAD_RATE_LIMIT_IP) and ((timestamp - o[THREAD_RATE_LIMIT_IP-1].timestamp) < datetime.timedelta(hours = 1./THREAD_RATE_LIMIT_IP)):
            return "You're posting to fast. {0} new threads per hour allowed. Please, wait {1} before posting.".format(THREAD_RATE_LIMIT_IP, str(datetime.timedelta(hours = 1./THREAD_RATE_LIMIT_IP) - (timestamp - o[THREAD_RATE_LIMIT_IP-1].timestamp)))
        elif (h.count()>=THREAD_RATE_LIMIT) and ((timestamp - h[THREAD_RATE_LIMIT-1].timestamp) < datetime.timedelta(hours = 1./THREAD_RATE_LIMIT)):
            return "Global wipe protection. Please, wait before posting."
        else:
            return False
    except:
        return False

def check_replying(ip, timestamp, threadnum, boardname):
    Posts = get_board(boardname)
    q = Posts.select().where( (Posts.parent == threadnum) & (Posts.ip == ip) ).order_by(Posts.timestamp.desc()).limit(1)
    try:
        if (timestamp - q[0].timestamp) < datetime.timedelta(minutes = 1./REPLY_RATE_LIMIT):
            return "You're replying to fast"
        else:   
            return False
    except:
        return False

def check_banned(user_ip):
    try:
        q = Bans.get( (Bans.ip_start <= user_ip) & (Bans.ip_stop >= user_ip))
        if q.expires > datetime.datetime.now():
            return True
        else:
            q.delete_instance() 
    except Exception, e:
        print e
        return False

def upload_to_imgur(imgfile):
    img_data = {}
    try:
        uploaded_image = im.upload_image(binary=imgfile)
        img_data = {"link": uploaded_image.link, "deletehash": uploaded_image.deletehash,
                 "size": uploaded_image.size, "width": uploaded_image.width, "height": uploaded_image.height }
        return json.dumps(img_data)
    except Exception, e:
        raise Exception

def tornado_run():
    import tornado.platform.twisted
    tornado.platform.twisted.install()
    import tornado.httpserver
    import tornado.wsgi
    import tornado.ioloop
    bottle_handler = tornado.wsgi.WSGIContainer(bottle_app)
    tornado.httpserver.HTTPServer(bottle_handler).listen(SERVER_PORT, SERVER_HOST)
    if USE_IRC_BOT:
        import botik
        from twisted.internet import reactor
        from http_proxy_connect import HTTPProxyConnector

        factory = botik.Irc_Connect_Factory()

        global ircbot
        ircbot = factory

        if BOT_PROXY:
            proxy = HTTPProxyConnector( BOT_HTTP_PROXY_HOST, BOT_HTTP_PROXY_PORT)
            proxy.connectTCP( BOT_HOST, BOT_PORT, factory )
        else:
            reactor.connectTCP( BOT_HOST, BOT_PORT, factory )
        tornado.ioloop.IOLoop.instance().start()
    else:
        tornado.ioloop.IOLoop.instance().start()

if __name__ == "__main__":
    if USE_MEMCACHED:
        import pylibmc
        mem_cache = pylibmc.Client(["127.0.0.1"], binary=True)#, behaviors={"tcp_nodelay": True, "ketama": True})
    database.connect()
    init_admins()
    if USE_IRC_BOT:
        import botik
        from twisted.web.server import Site
        from twisted.web import server, resource, http
        from twisted.internet import reactor
        from twisted.web.wsgi import WSGIResource
        from http_proxy_connect import *

        resource = WSGIResource(reactor, reactor.getThreadPool(), bottle_app)
        site = Site(resource)
        reactor.listenTCP(SERVER_PORT, site)

        factory = botik.Irc_Connect_Factory()

        global ircbot
        ircbot = factory

        if BOT_PROXY:
            proxy = HTTPProxyConnector( BOT_HTTP_PROXY_HOST, BOT_HTTP_PROXY_PORT)
            proxy.connectTCP( BOT_HOST, BOT_PORT, factory )
        else:
            reactor.connectTCP( BOT_HOST, BOT_PORT, factory )

        reactor.run()

    if SERVER == "tornado":
        tornado_run()
    else:
        bottle_app.run(host=SERVER_HOST, port=SERVER_PORT, debug=True)
else:
    if USE_MEMCACHED:
        import pylibmc
        mem_cache = pylibmc.Client(["127.0.0.1"], binary=True)#, behaviors={"tcp_nodelay": True, "ketama": True})
    # Mod WSGI launch
    init_admins()
    os.chdir(os.path.dirname(__file__))
    application = bottle_app
