# -*- coding: utf-8 -*-
#!/usr/bin/python

from twisted.words.protocols import irc
from twisted.internet import reactor, protocol
from http_proxy_connect import *
from config import *

class Irc_Client(irc.IRCClient):

    def connectionMade(self):
        self.nickname = BOT_NICKNAME
        self.channel = BOT_CHANNEL
        irc.IRCClient.connectionMade(self)
        self.factory.instance = self

    def connectionLost(self, reason):
        irc.IRCClient.connectionLost(self, reason)

    def signedOn(self):
        self.join(self.channel)

    def joined(self, channel):
        self.msg(self.channel, "\001ACTION Вещает и силён и смел \001")

    def send_stream(self, msg):
        self.msg(self.channel, msg)

class Irc_Connect_Factory(protocol.ClientFactory):

    def __init__(self):
        self.instance = ''
        self.maxtries = 5
        self.tries = 0

    def buildProtocol(self, addr):
        self.p = Irc_Client()
        self.p.factory = self
        return self.p

    def clientConnectionLost(self, connector, reason):
        try:
            connector.connect()
            self.tries = 0
        except:
            self.tries += 1
            if self.tries == self.maxtries:
                reactor.stop()
    
    def clientConnectionFailed(self, connector, reason):
        print "IRC connection failed:", reason
        #connector.connect()
        reactor.stop()

    def get_instance(self):
        return self.instance

def init_bot():
    factory = Irc_Connect_Factory()
    if BOT_PROXY:
        proxy = HTTPProxyConnector( BOT_HTTP_PROXY_HOST, BOT_HTTP_PROXY_PORT)
        proxy.connectTCP( BOT_HOST, BOT_PORT, factory )
    else:
        reactor.connectTCP( BOT_HOST, BOT_PORT, factory )
    reactor.run()

if __name__ == "__main__":
    init_bot()