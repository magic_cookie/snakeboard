#!/usr/bin/python
# -*- coding: utf-8 -*-

from peewee.peewee import *
from db_model import *
from config import BOARDS

database.connect()
try:
    Boards.create_table()
    print "table Boards created"
except Exception, e:
    print e
    pass

try:
    Bans.create_table()
    print "table Bans created"
except Exception, e:
    print e
    pass

try:
    Users.create_table()
    print "table Users created"
except Exception, e:
    print e
    pass        

try:
    Invites.create_table()
    print "table Invites created"
except Exception, e:
    print e
    pass

for b in BOARDS:
    try:
        create_board(b)
        Boards.create( board = b, description = BOARDS[b]["description"], owner = BOARDS[b]["owner"] )
        print "table", b, "created"
    except Exception, e:
        print e
        pass

print "OK..."