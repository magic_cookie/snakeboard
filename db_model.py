#!/usr/bin/python
# -*- coding: utf-8 -*-


from peewee.peewee import *
import datetime
from config import database, TABLE_PREFIX

def create_board(name = ""):
        get_board(name).create_table()

def get_board(name = "", table_prefix = TABLE_PREFIX):

    name = table_prefix+name

    class Posts(Model):

        parent = IntegerField()
        timestamp = DateTimeField(default=datetime.datetime.now())
        bumped = DateTimeField(default=datetime.datetime.now())
        ip = CharField()
        name = CharField()
        subject = CharField(default='')
        message = TextField()
        imagedata = CharField()

        def __unicode__(self):
            return '%s: %s' % (self.name, self.message)

        class Meta:
            database = database
            db_table = name

    return Posts

def answers_count(boardname, thread_id):
    return get_board(boardname).select().where(get_board(boardname).parent == thread_id).count()

def threads_count(boardname):
    return get_board(boardname).select().where(get_board(boardname).parent == 0).count()

def banned_count():
    return Bans.select().count()

def invites_count():
    return Invites.select().count()

def users_count():
    return Users.select().count()

class Boards(Model):

    board = CharField()
    description = CharField()
    owner = CharField(default='')

    def __unicode__(self):
        return '%s: %s: %s' % (self.board, self.description, self.owner)

    class Meta:
        database = database

class Bans(Model):

    ip_start = CharField()
    ip_stop = CharField()
    expires = DateTimeField(default=datetime.datetime.now())

    class Meta:
        database = database

class Users(Model):

    login = CharField()
    userhash = CharField()
    special = CharField(default="")
    #invites

    def __unicode__(self):
        return '%s: %s: %s' % (self.login, self.userhash, self.special)

    class Meta:
        database = database

class Invites(Model):

    invitecode = CharField()
    taken = BooleanField()
    expires = DateTimeField(default=datetime.datetime.now()+datetime.timedelta(days=30))

    def __unicode__(self):
        return '%s' % (self.invitecode) 

    class Meta:
        database = database


class Test():

    def __init__(self):
        database.connect()
        self.username = "Roy Batty"
        self.post = "I've seen things you people wouldn't believe. Attack ships on fire off the shoulder of Orion. I watched C–beams glitter in the darkness at Tannhauser Gate. All those moments will be lost in time like tears in rain."

    def add(self):
        Posts.create(username = self.username, post = self.post)

    def update_post(self):
        update_query = Posts.update(post = "Post changed by update_post() function").where(Posts.username == self.username)
        update_query.execute()

    def rem(self):
        p = Posts.get(Posts.username == self.username)
        p.delete_instance()


if __name__ == "__main__":
    t = Test()
    #t.add()
    #t.update_post()
    #t.rem()