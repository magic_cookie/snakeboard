var e_prefix = ENGINE_JSPREFIX || "";

$(document).on('click', '#banthem', function (e) {
    e.preventDefault();
    ip_start = $("#start_ip").val();
    ip_stop = $("#stop_ip").val();
    $.ajax({
        url: e_prefix+"/x/ban",
        type: "POST",
        data: JSON.stringify({ "ip_start": ip_start, "ip_stop": ip_stop, "exp": "never"}),
        contentType: "application/json",
        dataType: "json",
        success: function(data) {
            console.log(data.status);
            $("#suda").html(data.status);
        }
    });
});

$(document).on('click', '#remsel', function (e) {
    e.preventDefault();
    for (var i in $("input:checked").attr("name") ){
    var ban_id = $("input:checked").eq(i).attr("name");
    $.ajax({
        url: e_prefix+"/x/remban",
        type: "POST",
        data: JSON.stringify({ "ban_id": ban_id}),
        contentType: "application/json",
        dataType: "json",
        success: function(data) {
            console.log(data.status);
            $("#suda").html(data.status);
        }
    });
    }
});