var e_prefix = ENGINE_JSPREFIX || "";

$(document).on('click', '#addinv', function (e) {
    e.preventDefault();
    var gencode = makeinv();
    $.ajax({
        url: e_prefix+"/x/addinv",
        type: "POST",
        data: JSON.stringify({ "todo": "generate", "invitecode": gencode}),
        contentType: "application/json",
        dataType: "json",
        success: function(data) {
            console.log(data.status);
            $("#suda").html(data.status);
        }
    });
});

$(document).on('click', '#remsel', function (e) {
    e.preventDefault();
    for (var i in $("input:checked").attr("name") ){
    var code = $("input:checked").eq(i).attr("name");
    $.ajax({
        url: e_prefix+"/x/addinv",
        type: "POST",
        data: JSON.stringify({ "todo": "remove", "invitecode": code}),
        contentType: "application/json",
        dataType: "json",
        success: function(data) {
            console.log(data.status);
            $("#suda").html(data.status);
        }
    });
    }
});


function makeinv()
{
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for( var i=0; i < 20; i++ )
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}