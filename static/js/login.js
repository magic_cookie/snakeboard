var e_prefix = ENGINE_JSPREFIX || "";
$(document).on('click','#submit', function (e) {
	e.preventDefault();
	var login = $.trim($("#name").val());
	var pass = $("#pass").val();
	l(login, pass);
});

$(document).on('click','#anonlogin', function (e) {
	e.preventDefault();
	if ($('#tickme').is(':checked')){
		$('#name').val('posthuman');
		l('posthuman', '');
 	}
 	else {};
});

function l(login, pass){
	var shaObj = new jsSHA(login+pass, "TEXT");
	var hmac = shaObj.getHMAC("hidden", "TEXT", "SHA-512", "HEX");
	console.log(hmac);
	document.cookie="u="+hmac;
    $(location).attr('href', e_prefix+'/');
}