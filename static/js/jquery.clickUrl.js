$.fn.clickUrl = function() {
    var regexp = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gi;
    this.each(function() {
        $(this).html(
            $(this).html().replace(regexp,'<a href="$1">$1</a>')
        );
    });
    return $(this);
}