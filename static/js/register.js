var e_prefix = ENGINE_JSPREFIX || "";
$(document).on('click','#submit', function (e) {
	e.preventDefault();
	var login = $.trim($("#name").val());
	var pass = $("#pass").val();
	var repass = $("#repass").val();
	var invite = $.trim($("#invite").val());
	if(pass == repass){
        var shaObj = new jsSHA(login+pass, "TEXT");
        var hmac = shaObj.getHMAC("hidden", "TEXT", "SHA-512", "HEX");
        alert(hmac);
        document.cookie="u='"+hmac+"'";
        $.ajax({
            url: e_prefix+"/x/reguser",
            type: "POST",
            data: JSON.stringify({ "login": login, "userhash": hmac, "invitecode": invite}),
            contentType: "application/json",
            dataType: "json",
            success: function(data) {
                console.log(data.status);
                $("body").html(data.status);
                $(location).attr('href', e_prefix+'/');
            }
        });
	}
    else{ $("body").html("Passwords don't match"); }
});


