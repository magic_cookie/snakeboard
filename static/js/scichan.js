var e_prefix = ENGINE_JSPREFIX || "";

function wrapText(elementID, openTag, closeTag) {
    var textArea = $('#' + elementID);
    var len = textArea.val().length;
    var start = textArea[0].selectionStart;
    var end = textArea[0].selectionEnd;
    var selectedText = textArea.val().substring(start, end);
    var replacement = openTag + selectedText + closeTag;
    textArea.val(textArea.val().substring(0, start) + replacement + textArea.val().substring(end, len));
}

$(document).ready(function() {
    $(".fancybox").fancybox();
    $(".post").clickUrl(); 
    $(".response").clickUrl();
});

$(document).on('click', '.reflink', function (e) {
    e.preventDefault();
    $('#txtarea').insertAtCaret(">>"+$(this).html().slice(1)+' ');
});
$(document).on('click', 'up', function () {
    $('html, body').animate({scrollTop:0}, 800);
    $('up').hide(800, function(){ $('up').remove(); });
});
$(document).on('click', '.f_b, .f_i, .f_u, .f_s, .f_tex, .f_ltex', function (e) {
    var tag = $(this).attr('class').split(' ')[1].substring(2)
    wrapText('txtarea', '['+tag+']', '[/'+tag+']');
});
$(document).on("mouseover", ".getbox", function()
{});

$('#imginput').bind('change', function() {
    $("#warn").html("");
    if(this.files[0].size>8000000){
        $("#warn").html("Image size should be < 8 MB");
    }
});

$(document).on('click', '#logout_profile', function () {
    document.cookie="u=''; expires='Thu, 01-Jan-70 00:00:01 GMT;'";
    $(location).attr('href', e_prefix+'/');
});

$(document).on('click', '#profile', function () {
    alert("netooo");
});