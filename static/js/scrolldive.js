function isLocalStorageAvailable() {
    try {
        return 'localStorage' in window && window['localStorage'] !== null;
    } catch (e) {
        return false;
    }
}

function set_storage(k,v){
try {
    localStorage.setItem(k, v);
} catch (e) {
    if (e == QUOTA_EXCEEDED_ERR) {
        alert('Локальное хранилище переполнено');
    }
}
}

var s_trigger = true;
var math = document.getElementById("MathExample");
$(function() {
    $(document).endlessScroll({
    	inflowPixels:25,
        fireOnce: true,
        fireDelay: false,
        ceaseFireOnEmpty: true,
        content: function(i,p,d){if(s_trigger){get_data(i,p,d); return ' ';}else{return '';}}
    });
});

function get_data(page,p,d) {
if(page==1){$('<up><i class="icon-chevron-up icon-2x"></i></up>').appendTo('body').show(800); localStorage.clear(); }
var more_href = "more/" + page.toString();
var key = "page_"+page.toString()
var stored = localStorage.getItem(key);
if (!stored){
$.get(more_href, function(data) {
    if(data.length<=1){s_trigger=false;return true;}
    set_storage(key, data);
    $(".threads_section").append(data);
    MathJax.Hub.Queue(["Typeset",MathJax.Hub,math]);
});
var m1 = "more/" + (page+1).toString();
var k1 = "page_"+(page+1).toString()
$.get(m1, function(data) { //preload
    if(data.length<=1){s_trigger=false;return true;}
    set_storage(k1, data);

});
}
else{
    $(".threads_section").append(stored);
    MathJax.Hub.Queue(["Typeset",MathJax.Hub,math]);
}
}
